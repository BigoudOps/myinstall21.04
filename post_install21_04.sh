#!/bin/bash
rouge='\e[1;31m'
neutre='\e[0;m'
vert='\e[4;32m'
bleu='\e[1;34m'
jaune='\e[1;33m'
if [ "$UID" -eq "0" ]; then
    echo -e "${rouge}lance le sans sudo, le mot de passe sera demandé dans le terminal lors de la 1ère action nécessitant le droit administrateur.${vert}"
    exit
fi
sudo apt install -y apt-transport-https gnupg-agent software-properties-common
if [ ! -f "/usr/bin/wget" ]; then
    echo -e "${vert}installation de wget pour l'installation des paquets deb${neutre}"
    sudo apt install wget
else
    echo -e "${rouge}wget est déjà installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/git" ]; then
    echo -e "${rouge} Voulez-vous installer git?${neutre}[O/n]"
    read -r git
    case $git in 
    N | n)
        echo "Git ne sera pas installé."
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${vert}installation de Git${neutre}"
        sudo apt install -y git 
        ;;
    esac
else
    echo -e "${rouge}Git est déjà installé ${neutre}"
    echo "le script continue la verification et l'installation des programmes"

fi
if [ ! -f "/etc/ca-certificates" ]; then
    sudo apt install -y ca-certificates
else
    echo -e "${rouge}ca-certificateq est déjà installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/curl" ]; then
    echo -e "${bleu}installation de curl ${neutre}"
    sudo apt install -y curl
else
    echo -e "${rouge}curl est déjà installé ${neutre}"
    echo "le script continue la verification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/gnome-tweaks" ]; then
    echo -e "${jaune}Voulez-vous installer gnome-tweaks?${neutre}[O/n]"
    read -r -r gnome_tweaks
    case $gnome_tweaks in
    N | n)
        echo "Gnome-tweaks ne sera pas installé."
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${jaune}installation de gnome-tweaks${neutre}"
        sudo apt install gnome-tweaks
        ;;
    esac
else
    echo -e "${rouge}gnome-tweaks est déjà installé ${neutre}"
    echo "le script continue la verification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/codium" ]; then
    echo -e "${bleu}voulez-vous installer VSCodium ?${neutre}[O/n]"
    read -r -r codium
    case $codium in
    N | n)
        echo "VSCodium ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${bleu}installation de VSCodium ${neutre}"
        wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium.gpg
        echo 'deb https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list
        sudo apt update && sudo apt install codium
        ;;
    esac
else
    echo -e "${rouge}VSCodium est déjà installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/docker" ]; then
    echo -e "${vert}voulez-vous installer docker?${neutre}[O/n]"
    read -r -r docker
    case $docker in
    N | n)
        echo "Docker ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${vert}installation de docker ${neutre}"
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
        sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose
        sudo usermod -aG docker "$USER"
        ;;
    esac
else
    echo -e "${rouge}docker est déjà installé ${neutre}"
    echo "le script continue la verification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/ansible" ]; then
    echo -e "${bleu}voulez-vous installer ansible?${neutre}[O/n]"
    read -r -r ansible
    case $ansible in
    N | n)
        echo "Ansible ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${bleu}installation de ansible${neutre}"
        sudo apt-add-repository --yes --update ppa:ansible/ansible
        sudo apt install ansible
        ;;
    esac
else
    echo -e "${rouge}ansible est déjà installé ${neutre}"
    echo "le script continue la verification et l'installation des programmes"
fi
if [ ! -f "/bin/gdebi" ]; then
    echo -e "${jaune}voulez-vous installer l'utilitaire gdebi permettant l'installation de paquet deb?${neutre}[O/n]"
    read -r gdebi
    case $gdebi in
    N | n)
        echo "L'utilitaire gdebi ne sera pas installé."
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${jaune}installation de l'utilitaire gdebi permettant l'installation de paquet deb${neutre}"
        sudo apt install gdebi-core
        ;;
    esac
else
    echo -e "${rouge}Gdebi est déjà installé${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/chrome-gnome-shell" ]; then
    echo -e "${vert}voulez-vous installer le paquet chrome-gnome-shell?${neutre}[O/n]"
    read -r gnome_shell
    case $gnome_shell in
    N | n)
        echo "Le paquet chrome-gnome-shell ne sera pas installé."
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${vert}installation du paquet chrome-gnome-shell${neutre}"
        sudo apt install chrome-gnome-shell
        ;;
    esac
else
    echo -e " ${vert}chrome-gnome-shell est déjà installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/mumble" ]; then
    echo -e "${bleu}mumble n'est pas installé voulez-vous l'installer?${neutre}[O/n]"
    read -r mumble
    case $mumble in
    N | n)
        echo "mumble ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo "${bleu}installation de mumble${neutre}"
        sudo apt install mumble
        ;;
    esac
else
    echo -e "${rouge}mumble est déjà installé${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/vlc" ]; then
    echo -e "${bleu}VLC n'est pas installé voulez-vous l'installer?${neutre}[O/n]"
    read -r vlc
    case $vlc in
    N | n)
        echo -e "VLC ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo "${jaune}installation de vlc${neutre}"
        sudo apt install vlc
        ;;
    esac
else
    echo -e "${rouge}vlc est déjà installé${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/discord" ]; then
    echo -e "Discord n'est pas installé voulez-vous l'installer? [O/n]"
    read -r discord
    case $discord in
    N | n)
        echo -e "Discord ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${bleu}installation de discord${neutre}"
        wget -O ~/discord.deb "https://discordapp.com/api/download?platform=linux&format=deb"
        sudo gdebi ~/discord.deb
        ;;
    esac
else
    echo -e "${rouge}Discord est déjà installé${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/games/lutris" ]; then
    echo -e "${vert}Lutris n'est pas installé voulez-vous l'installer?${neutre} [O/n]"
    read -r lutris
    case $lutris in
    N | n)
        echo "Lutris ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${vert} installation de Lutris ${neutre}"
        sudo add-apt-repository ppa:lutris-team/lutris
        sudo apt update
        sudo apt install lutris
        ;;
    esac
else
    echo -e "${rouge}Lutris est déjà installé${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/kodi" ]; then
    echo -e "${jaune}Voulez-vous installer kodi ?${neutre} [O/n]"
    read -r -r kodi
    case $kodi in
    N | n)
        echo "kodi ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${jaune} installation de kodi${neutre}"
        sudo add-apt-repository --yes --update ppa:team-xbmc/ppa
        sudo apt install kodi
        ;;
    esac
else
    echo -e "${rouge}Kodi est déjà installé${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/sbin/synaptic" ]; then
    echo -e "${bleu}Voulez-vous installer synaptic? ${neutre}[O/n]"
    read -r synaptic
    case $synaptic in
    N | n)
        echo "synaptic ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${bleu} installation de synaptic manager${neutre}"
        sudo apt install synaptic
        ;;
    esac
else
    echo -e "${rouge}synaptic manager est déjà installé${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/sushi" ]; then
    echo -e "${jaune}voulez-vous installer gnome-sushi ?${neutre}[O/n]"
    read -r gnome_sushi
    case $gnome_sushi in
    N | n)
        echo "gnome-sushi ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${jaune}installation de gnome-sushi ${neutre}"
        sudo apt install gnome-sushi
        ;;
    esac
else
    echo -e "${rouge}gnome-sushi est déjà installé${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/keepassxc" ]; then
    echo -e "${jaune}keepassxc n'est pas installé voulez-vous l'installer?${neutre}[O/n]"
    read -r keepassxc
    case $keepassxc in
    N | n)
        echo "keepassxc ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${jaune}installation du ppa keepassxc ${neutre}"
        sudo add-apt-repository --yes --update ppa:phoerious/keepassxc
        sudo apt install keepassxc
        ;;
    esac
else
    echo -e "${rouge}keepassxc est déjà installé${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/pulseeffects" ]; then
    echo -e "${bleu}PulseEffects n'est pas installé voulez-vous l'installer?${neutre}[O/n]"
    read -r pulse
    case $pulse in
    N | n)
        echo "PulseEffects ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${bleu}installation du ppa PulseEffects ${neutre}"
        sudo add-apt-repository --yes --update ppa:mikhailnov/pulseeffects -y
        sudo apt install pulseeffects pulseaudio --install-recommends
        sudo apt-get install pulseeffects lsp-plugins -y
        ;;
    esac
else
    echo -e "${rouge}Pulseeffects est déjà installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/virtualbox" ]; then
    echo -e "${jaune}Virtualbox n'est pas installé voulez-vous l'installer?${neutre}[O/n]"
    read -r virtual
    case $virtual in
    N | n)
        echo "Virtualbox ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${jaune}installation de Virtualbox ${neutre}"
        sudo apt install virtualbox -y
        sudo apt install virtualbox-ext-pack
        ;;
    esac
else
    echo -e "${rouge}Virtualbox est déjà installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/virt-manager" ]; then
    echo -e "${vert}Qemu n'est pas installé voulez-vous l'installer? ${neutre}[O/n]"
    read -r qemu
    case $qemu in
    N | n)
        echo "Qemu ne sera pas installé"
        echo "le script continue la verification et l'installation des programmes..."
        ;;
    O | o |*)
        echo -e "${vert}Installation de Qemu${neutre}"
        sudo apt install -y qemu qemu-kvm libvirt-daemon libvirt-clients bridge-utils virt-manager
        sudo systemctl enable --now libvirtd # enable on boot
        sudo apt install uvtool 
        ;;
    esac
else
    echo -e "${rouge}Qemu est déjà installé ${neutre}"
    echo "le script continue la verification et l'installation des programmes"
fi

if [ ! -f "/usr/bin/shellcheck" ]; then
    echo -e "${vert}ShellCheck n'est pas installé voulez-vous l'installer?${neutre}[O/n]"
    read -r shell
    case $shell in
    N | n)
        echo "ShellCheck ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${vert}installation de ShellCheck ${neutre}"
        sudo apt install shellcheck -y
        ;;
    esac
else
    echo -e "${rouge}ShellCheck est déja installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/peek" ]; then
    echo -e "${bleu}Peek n'est pas installé voulez-vous l'installer? ${neutre}[O/n]"
    read -r peek
    case $peek in
    N | n)
        echo "Peek ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${bleu}installation de Peek ${neutre}"
        sudo add-apt-repository --yes --update ppa:peek-developers/stable
        sudo apt install peek
        ;;
    esac
else
    echo -e "${rouge}Peek est déja installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/adb" ]; then
    echo -e "${bleu}ADB n'est pas installé voulez-vous l'installer?${neutre}[O/n]"
    read -r adb
    case $adb in
    N | n)
        echo "ADB ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${bleu}Installation de ADB ${neutre}"
        sudo apt install android-tools-adb -y
        ;;
    esac
else
    echo -e "${rouge}ADB est déjà installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/fastboot" ]; then
    echo -e "${bleu}Fastboot n'est pas installé voulez-vous l'installer? ${neutre}[O/n]"
    read -r fastboot
    case $fastboot in
    N | n)
        echo "Fastboot ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        
        ;;
    O | o | *)
        echo -e "${bleu}Installation de Fastboot ${neutre}"
        sudo apt install android-tools-fastboot -y
        ;;
    esac
else
    echo -e "${rouge}Fastboot est déjà installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/kazam" ]; then
    echo -e "${jaune}Kazam n'est pas installé voulez-vous l'installer?${neutre}[O/n]"
    read -r kazam
    case $kazam in
    N | n)
        echo "Kazam ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        
        ;;
    O | o | *)
        echo -e "${jaune}Installation de Kazam${neutre}"
        sudo apt install kazam -y
        ;;
    esac
else
    echo -e "${rouge}Kazam est déjà installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/etc/apt/sources.list.d/numix-ubuntu-ppa-hirsute.list" ]; then
    echo -e "${vert} Voulez-vous installer le depot ppa de numix ${neutre}[O/n] ?"
    read -r Numixppa
    case $Numixppa in
    N | n)
        echo -e " numix ne sera pas installé " 
        ;;
    O | o | *)
        echo -e " Installation du dépôt numix"
        sudo add-apt-repository --yes --update ppa:numix/ppa
        sudo apt install numix-icon-theme-circle
        sudo apt install numix-blue-gtk-theme
        sudo apt install numix-gtk-theme
        ;;
    esac
else
    echo -e "${bleu}le dépôt ppa de Numix est déjà installé ${neutre}"
    echo -e "${bleu}Voulez-vous installer des themes Numix ?${neutre}[O/n]"
    read -r Numixthemes
    case $Numixthemes in
    N | n)
        echo -e "${rouge}Les thèmes Numix ne seront pas installés ${neutre}"
        ;;
    O | o | *)
        echo -e "Installation des thèmes Numix"
        sudo apt install numix-icon-theme-circle
        sudo apt install numix-blue-gtk-theme
        sudo apt install numix-gtk-theme
        ;;
    esac

    echo "le script continue la vérification et l'installation des programmes"

fi

if [ ! -f "/usr/bin/ckb-next" ]; then
    echo -e " ${bleu}Voulez-vous installer ckb-next depuis le dépot PPA? ${neutre}[O/n]"
    read -r ckb
    case $ckb in
    N | n)
        echo "ckb-next ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo "Installation de ckb-next"
        sudo add-apt-repository --yes --update ppa:tatokis/ckb-next
        sudo apt install ckb-next
        ;;
    esac
fi
exit

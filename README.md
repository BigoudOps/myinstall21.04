![visitors](https://visitor-badge.glitch.me/badge?page_id=BigoudOps.readme)

[![Join the Discord channel](https://img.shields.io/static/v1.svg?label=%20Rejoignez-moi%20sur%20Discordl&message=%F0%9F%8E%86&color=7289DA&logo=discord&logoColor=white&labelColor=2C2F33)](https://discord.gg/bfB6Ve6) 
[![Reddit profile](https://img.shields.io/reddit/subreddit-subscribers/apdm?style=social)](https://www.reddit.com/r/apdm) 

# myinstall21.04

just a script for a post install Ubuntu 21.04 in this repository you can find this software ⬇:

by category

💼 :

[Ansible](https://docs.ansible.com/ansible/latest/index.html)

[Docker](https://docs.docker.com/get-started/overview/)

[Gdebi](https://launchpad.net/gdebi)

[Synaptic](http://www.nongnu.org/synaptic/)

[Virtualbox](https://www.virtualbox.org/)

[Virtual Machine Manager](https://virt-manager.org/)

🥂 :

[Mumble](https://www.mumble.com/)

[Discord](https://discord.com/)

🎮 :

[Lutris](https://lutris.net/) 

📺 :

[Vlc](https://www.videolan.org/) 

[kodi](https://kodi.tv/)

[Peek](https://github.com/phw/peek)

[Kazam](https://launchpad.net/kazam)

🛠 :

[gnome-tweaks](https://gitlab.gnome.org/GNOME/gnome-tweaks)

[gnome-sushi](https://en.wikipedia.org/wiki/GNOME_sushi)

[numix themes](https://numixproject.github.io/)

[PulseEffects](https://github.com/wwmm/pulseeffects)

[ShellCheck](https://github.com/koalaman/shellcheck)

🔐 :

[KeepassXC](https://keepassxc.org/)

⌨ :

[VSCodium](https://vscodium.com/)

[ADB and Fastboot](https://www.android.com/)

[CKB-next](https://github.com/ckb-next/ckb-next)
